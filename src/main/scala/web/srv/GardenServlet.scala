package web.srv

import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import db.record.Garden

/**
 * Created with IntelliJ IDEA.
 * User: Oleg
 * Date: 11.11.12
 * Time: 4:17
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "Garden", urlPatterns = Array("/garden","/garden/*"))
class GardenServlet extends HttpServlet with EntityRestServlet[Garden] {
  def entityMeta = Garden
}
