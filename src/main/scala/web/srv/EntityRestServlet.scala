package web.srv

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import java.io.InputStream
import net.liftweb.mongodb.record.{MongoMetaRecord, MongoRecord}
import net.liftweb.json._
import io.{Codec, Source}
import java.nio.charset.Charset
import net.liftweb.json.JsonAST.JValue
import net.liftweb.common.{ParamFailure, Failure, Empty, Full}

/**
 * Created with IntelliJ IDEA.
 * User: odomontois
 * Date: 10.09.12
 * Time: 10:50
 * To change this template use File | Settings | File Templates.
 */
trait EntityRestServlet[RecordType <: MongoRecord[RecordType]] {
  this: HttpServlet =>

  import ReaderUtils._

  def entityMeta: MongoMetaRecord[RecordType]

  override def doPost(req: HttpServletRequest, resp: HttpServletResponse) {
    import resp._
    setContentType(ctJSON)
    setCharacterEncoding(UTF_8)

    def postOne(obj: JObject) = List(entityMeta fromJValue (obj) openOr (throw new BadRequestException(obj)) save) map (_ asJValue)

    def postMany(list: List[JValue]): List[JValue] = list flatten ({
      case JArray(values) => postMany(values)
      case obj: JObject => postOne(obj)
      case _ => throw new Exception()
    })

    getWriter println (
      pretty(render(
        JArray(
          parse(req.getInputStream) match {
            case JArray(array) => postMany(array)
            case obj: JObject => postOne(obj)
            case _ => throw new Exception()
          }))))
  }

  override def doPut(req: HttpServletRequest, resp: HttpServletResponse) {
    println(req.getCharacterEncoding)
    req.setCharacterEncoding("cp1251")
    val data: String = req.getInputStream
    println(data)
    val jValue = parse(data)
    entityMeta.fromJValue(jValue) openOr (throw new BadRequestException(jValue)) save
  }

  override def doDelete(req: HttpServletRequest, resp: HttpServletResponse) {
    entityMeta.find(req.getInputStream) openOr (throw new NotFoundException) delete_!
  }

  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    import resp._
    setContentType(ctJSON)
    setCharacterEncoding(UTF_8)
    val path = req.getServletPath
    val uri = req.getRequestURI
    val id = if (uri != path && uri.startsWith(path)) Some(uri.drop(path.length + 1)) else None
    getWriter print (
      pretty(render(
        id match {
          case None => JArray(entityMeta.findAll map (_ asJValue))
          case Some(id) => entityMeta.find(id) match {
            case Full(record) => record.asJValue
            case _ => JString("No such gargen")
          }})))
  }
}

object ReaderUtils {
  val ctJSON = "application/JSON"
  val UTF_8 = "UTF-8"
  implicit val codec = new Codec(Charset forName UTF_8)

  implicit def inputStreamToString(istream: InputStream): String =
    Source.fromInputStream(istream) getLines() mkString
}

case class BadRequestException(value: JValue) extends Exception

class NotFoundException extends Exception
