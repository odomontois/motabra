package db.record
import net.liftweb.mongodb.record.field.{MongoCaseClassField, MongoCaseClassListField, StringPk}
import net.liftweb.mongodb.record.{MongoMetaRecord, MongoRecord}

class Garden private() extends MongoRecord[Garden] with StringPk[Garden] {

  import db.record.Garden._

  override def meta = Garden

  object blooms extends MongoCaseClassListField[Garden, Bloom](this)

  object gardenOptions extends MongoCaseClassField[Garden, Options](this)

}


object Garden extends Garden with MongoMetaRecord[Garden] {

  case class Bloom(x: Int, y: Int)

  object Options {

    case class PetalCount(min: Int, max: Int)

    case class PetalStretch(min: Float, max: Float)

    case class GrowFactor(min: Float, max: Float)

    case class BloomRadius(min: Float, max: Float)

    case class Color(min: Int, max: Int, opacity: Float)

  }

  import Options._

  case class Options(petalCount: PetalCount,
                     petalStretch: PetalStretch,
                     growFactor: GrowFactor,
                     bloomRadius: BloomRadius,
                     density: Int,
                     growSpeed: Float,
                     color: Color,
                     tanAngle: Int)

  implicit val implicitMeta = this
}