/**
 * Created with IntelliJ IDEA.
 * User: Oleg
 * Date: 15.09.12
 * Time: 0:35
 * To change this template use File | Settings | File Templates.
 */
public class TestShit {
    static class Parent {
        void method1() {
            System.out.println("Parent method1");
        }

        void method2() {
            System.out.println("Parent method2");
        }
    }

    static class Child extends Parent {
        @Override
        void method1() {
            super.method2();    //To change body of overridden methods use File | Settings | File Templates.
            System.out.println("Child method1");
        }

        @Override
        void method2() {
            super.method1();    //To change body of overridden methods use File | Settings | File Templates.
            System.out.println("Child method2");
        }
    }

    public static void main(String[] args) {
        final Child x = new Child();
        x.method1();
        x.method2();

    }
}
